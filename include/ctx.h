#include "list.h"
#include <stdlib.h>
#include <string.h>

#define MAX_CTX 256
#define STACK_SIZE 8192
#define STATE_NOT_STARTED 0
#define STATE_RUNNING 1
#define STATE_UNUSED 2

typedef void (*func_t ) (void *);


struct ctx_s {
    void* esp;
    void* ebp;
    char stack[STACK_SIZE];
    func_t f;
    void *args;
    int state;
};

int init_ctx(struct ctx_s*, func_t, void *);

int switch_to( struct ctx_s *);

int create_ctx(func_t, void *);

void yield();

void init_buffer();
void start_sched();

void f_pong(void *);
void f_ping(void *);