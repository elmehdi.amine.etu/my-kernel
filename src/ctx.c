// #include "ctx.h"
#include "list.h"
#include "idt.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CTX 32
#define STACK_SIZE 8192
#define STATE_NOT_STARTED 0
#define STATE_RUNNING 1
#define STATE_UNUSED 2
#define TIMER_IRQ 0

typedef void (*func_t ) (void *);


struct ctx_s {
    void* esp;
    void* ebp;
    char stack[STACK_SIZE];
    func_t f;
    void *args;
    int state;
	struct list_head myhandle;
};

struct sem_s {
	int value;
	struct list_head ctx_queue;
};

struct sem_s sem_ctx;

struct ctx_s buffer[MAX_CTX];

struct ctx_s *courant = NULL;
LIST_HEAD(contextes);

void init_buffer()
{
	int i;
	for (i = 0; i < MAX_CTX; i++)
	{
		buffer[i].state = STATE_UNUSED;
	}
}

int init_ctx(struct ctx_s *ctx, func_t f, void *args)
{
	ctx->esp = ctx->stack + STACK_SIZE - 4;
	ctx->ebp = ctx->stack + STACK_SIZE - 4;
	ctx->f = f;
	ctx->state = STATE_NOT_STARTED;
	ctx->args = args;
	return 0;
}
// returns the next available ctx from the buffer
struct ctx_s *get_free_ctx(){
	int i;
	for (i = 0; i < MAX_CTX; i++)
	{
		if (buffer[i].state == STATE_UNUSED) return &buffer[i];
	}
	return NULL;
}

void sem_init(struct sem_s *sem, int val) {
	sem->value = val;
	INIT_LIST_HEAD(&sem->ctx_queue);
}

int create_ctx(func_t f, void *args)
{
	struct ctx_s *ctx = get_free_ctx();
	init_ctx(ctx, f, args);
	INIT_LIST_HEAD(&ctx->myhandle);

	list_add(&ctx->myhandle, &contextes);
	// irq_enable();
	return 0;
}

void switch_to(struct ctx_s *ctx)
{
	// puts("switch_to\n");
	static struct ctx_s *courant = NULL;
	static struct ctx_s *newctx;

	newctx = ctx;
	if (courant != NULL)
	{
		asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1"
			: "=r"(courant->esp), "=r"(courant->ebp)
			:
			:);
	}

	asm("movl %0, %%esp" "\n\t" "movl %1, %%ebp"
		:
		: "r"(newctx->esp), "r"(newctx->ebp)
		:);
	courant = newctx;
	if (newctx -> state == STATE_NOT_STARTED)
	{
		newctx->state = STATE_RUNNING;
		newctx->f(newctx->args);
	}
	// irq_enable();
}

// Yields the next context in the list
void yield() {
	irq_disable();
	// sem_down(&sem_ctx);
	// struct ctx_s *next;
	if (courant == NULL)
	{
		courant = list_first_entry(&contextes, struct ctx_s, myhandle);
		switch_to(courant);
	}
	else
	{
		if (courant == list_last_entry(&contextes, struct ctx_s, myhandle))
		{
			courant = list_first_entry(&contextes, struct ctx_s, myhandle);
		}
		else
		{
			courant = list_next_entry(courant, myhandle);
		}
		switch_to(courant);
	}
	irq_enable();
	// sem_up(&sem_ctx);
}
void sem_down(struct sem_s *sem) {
	irq_disable();
	// irq_enable();
	puts("sem down\n");
	sem->value--;
	if (sem->value < 0) {
		puts("sem down value < 0\n");
		list_add(&courant->myhandle, &sem->ctx_queue);
		switch_to(list_first_entry(&sem->ctx_queue, struct ctx_s, myhandle));
		yield();
		irq_enable();
	}
}
void sem_up(struct sem_s *sem) {
	irq_disable();
	puts("sem up\n");
	sem->value++;
	if (sem->value <= 0) {
		puts("sem up value <= 0\n");
		struct ctx_s *next = list_first_entry(&sem->ctx_queue, struct ctx_s, myhandle);
		list_del(&next->myhandle);
		switch_to(next);
		irq_enable();
	}
}

void context_handler(){
	yield();
}

void start_sched() {
	idt_setup_handler(TIMER_IRQ, &context_handler);
	sem_init(&sem_ctx, 1);
}

struct ctx_s ctx_pong;
struct ctx_s ctx_ping;


void f_ping(void *args)
{
	puts((char *) args);
	puts("\n");
	while (1)
	{
		// sem_down(&sem_ctx);
		puts("ping \n");
		yield();
		puts("ping\n");
		// sem_up(&sem_ctx);
	}
}

void f_pong(void *args)
{
	puts((char *) args);
	puts("\n");
	while (1)
	{
		// sem_down(&sem_ctx);
		puts("pong\n");
		yield();
		puts("pong\n");
		yield();
		puts("pong\n");
		// sem_up(&sem_ctx);
	}
}


// int main(void)
// {

// 	init_buffer();

// 	// sem_init(&sem_ctx, 1);
// 	create_ctx(f_ping, NULL);
// 	create_ctx(f_pong, NULL);
// 	start_sched();

// 	return 0;
// }