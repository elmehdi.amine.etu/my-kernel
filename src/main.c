#include <stdlib.h>
#include "ioport.h"
#include "gdt.h"
#include "idt.h"
#include "ctx.h"

void clear_screen();      /* clear screen */
void putc(char aChar);    /* print a single char on screen */
void puts(char *aString); /* print a string on the screen */
void puthex(int aNumber); /* print an Hex number on screen */

void empty_irq(int_regs_t *r)
{
}

/* multiboot entry-point with datastructure as arg. */

struct ctx_s ctx_pong;
struct ctx_s ctx_ping;

char keyboard_map(unsigned char c)
{
  switch (c)
  {
  case 0x10:
    return 'a';
  case 0x30:
    return 'b';
  case 0x2E:
    return 'c';
  case 0x20:
    return 'd';
  case 0x12:
    return 'e';
  case 0x21:
    return 'f';
  case 0x22:
    return 'g';
  case 0x23:
    return 'h';
  case 0x17:
    return 'i';
  case 0x24:
    return 'j';
  case 0x25:
    return 'k';
  case 0x26:
    return 'l';
  case 0x27:
    return 'm';
  case 0x31:
    return 'n';
  case 0x18:
    return 'o';
  case 0x19:
    return 'p';
  case 0x1E:
    return 'q';
  case 0x13:
    return 'r';
  case 0x1F:
    return 's';
  case 0x14:
    return 't';
  case 0x16:
    return 'u';
  case 0x2F:
    return 'v';
  case 0x2C:
    return 'w';
  case 0x2D:
    return 'x';
  case 0x15:
    return 'y';
  case 0x11:
    return 'z';

  default:
    break;
  }
  // returns the char corresponding to the scancode
  // if the scancode is not mapped, returns '\0'
  return c;
}
void handle_keyboard_input(){
  int i = 0;
  while (i == 0)
  {
    char c = _inb(0x60);
    char a = keyboard_map(c);
    if (a != '\0')
    {
      i = 1;
      putc(a);
    }
  }
}
void app_init()
{

  // init_buffer();

  // create_ctx(f_ping, "ping");
  // create_ctx(f_pong, "pong");

  // start_sched();
  handle_keyboard_input();
  // while (1)
  // {
  //   char c = _inb(0x60);
    
  //   putc(keyboard_map(c));
  //   // puthex(c);
  //   puts("\n");
  // }
}

void main()
{
  clear_screen();
  puts("Early boot.\n");
  puts("\t-> Setting up the GDT... ");
  gdt_init_default();
  puts("done\n");

  puts("\t-> Setting up the IDT... ");
  setup_idt();
  puts("OK\n");

  puts("\n\n");

  idt_setup_handler(0, empty_irq);
  idt_setup_handler(0, empty_irq);

  __asm volatile("sti");

  app_init();
}

/* base address for the video output assume to be set as character oriented by the multiboot */
unsigned char *video_memory = (unsigned char *)0xB8000;

/* clear screen */
void clear_screen()
{
  int i;
  for (i = 0; i < 80 * 25; i++)
  {                                           /* for each one of the 80 char by 25 lines */
    video_memory[i * 2 + 1] = 0x0F;           /* color is set to black background and white char */
    video_memory[i * 2] = (unsigned char)' '; /* character shown is the space char */
  }
}

/* print a string on the screen */
void puts(char *aString)
{
  char *current_char = aString;
  while (*current_char != 0)
  {
    putc(*current_char++);
  }
}

/* print an number in hexa */
char *hex_digit = "0123456789ABCDEF";
void puthex(int aNumber)
{
  int i;
  int started = 0;
  for (i = 28; i >= 0; i -= 4)
  {
    int k = (aNumber >> i) & 0xF;
    if (k != 0 || started)
    {
      putc(hex_digit[k]);
      started = 1;
    }
  }
}

/* print a char on the screen */
int cursor_x = 0; /* here is the cursor position on X [0..79] */
int cursor_y = 0; /* here is the cursor position on Y [0..24] */

void setCursor()
{
  int cursor_offset = cursor_x + cursor_y * 80;
  _outb(0x3d4, 14);
  _outb(0x3d5, ((cursor_offset >> 8) & 0xFF));
  _outb(0x3d4, 15);
  _outb(0x3d5, (cursor_offset & 0xFF));
}

void putc(char c)
{
  if (cursor_x > 79)
  {
    cursor_x = 0;
    cursor_y++;
  }
  if (cursor_y > 24)
  {
    cursor_y = 0;
    clear_screen();
  }
  switch (c)
  { /* deal with a special char */
  case '\r':
    cursor_x = 0;
    break; /* carriage return */
  case '\n':
    cursor_x = 0;
    cursor_y++;
    break; /* new ligne */
  case 0x8:
    if (cursor_x > 0)
      cursor_x--;
    break; /* backspace */
  case 0x9:
    cursor_x = (cursor_x + 8) & ~7;
    break; /* tabulation */
    /* or print a simple character */
  default:
    video_memory[(cursor_x + 80 * cursor_y) * 2] = c;
    cursor_x++;
    break;
  }
  setCursor();
}
